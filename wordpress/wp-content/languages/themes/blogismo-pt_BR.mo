��          �      �       0     1     K     d     �     �  	   �     �     �  8   �  1   �  3   &  (   Z  �  �     y  #   �  $   �      �        
   	       	     3   %  *   Y  3   �     �     
         	                                            API data are not yet set. All fields are required. E-mail address is not valid. Email is empty or invalid. Email:  Message:  Name:  Reply There was an error contacting the API, please try again. Unexpected error while attempting to send e-mail. You have successuffly subscribed to the newsletter. Your message was successfully submitted. Project-Id-Version: Blogismo
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-11-05 17:39+0100
PO-Revision-Date: 2016-01-20 18:49-0200
Last-Translator: DJMiMi <milutinjaric@gmail.com>
Language-Team: DJMiMi <milutinjaric@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;esc_attr__;esc_attr_e
X-Poedit-Basepath: .
Language: pt
X-Generator: Poedit 1.8.6
X-Poedit-SearchPath-0: C:/wamp/www/blogismo/wp-content/themes/blogismo
 Dados da API não definidos. Todos os campos são obrigatórios. Endereço de e-mail não é válido. E-mail está vazio ou inválido. E-mail:  Mensagem:  Nome:  Responder Ocorreu um erro ao contatar a API, tente novamente. Erro inesperado ao tentar enviar o e-mail. Você se inscreveu com sucesso na nossa newsletter. Mensagem enviada com sucesso. 