jQuery(document).ready(function($){
	"use strict";
	
	/*
	// transição ao abrir uma página - ampulheta + fundo verde
	$(window).load( function(){
		$('body').css('overflow','visible');
		$('.spinner-wrap').animate({top: '-1000px', opacity: 0}, 1000, function(){$(this).remove()});
	} );	
	*/

	//on scolling, show/animate timeline blocks when enter the viewport
	$(window).on('scroll', function(){		
		if( $(window).scrollTop() > 200 ){
			$('.to_top').fadeIn();
		}
		else{
			$('.to_top').fadeOut();
		}
	});
	
	$('.to_top').click(function(e){
		e.preventDefault();
		$("html, body").stop().animate(
			{
				scrollTop: 0
			}, 
			{
				duration: 1200
			}
		);		
	});	
	
	/* NAVIGATION */
	function handle_navigation(){
		if ($(window).width() >= 767) {
			$('ul.nav li.dropdown, ul.nav li.dropdown-submenu').hover(function () {
				$(this).addClass('open').find(' > .dropdown-menu').stop(true, true).hide().slideDown(200);
			}, function () {
				$(this).removeClass('open').find(' > .dropdown-menu').stop(true, true).show().slideUp(200);
	
			});
		}
		else{
			$('ul.nav li.dropdown, ul.nav li.dropdown-submenu').unbind('mouseenter mouseleave');
		}
	}
	handle_navigation();
	
	$(window).resize(function(){
		setTimeout(function(){
			handle_navigation();
		}, 200);
	});		

	
	/* PAGINATION */
	var calling = false;
	var max_in_row = parseInt( $('.max_in_row').val() );
	$(window).scroll(function(){
		if($(window).scrollTop() + $(window).height() > $(document).height() - $(window).height() / 2 && $('.load-more').length > 0 && calling === false) {
			calling = true;
			var $this = $('.load-more');
			var next_link = $this.val();
			var counter = $('.ajax-container .row:last .post-item').length;
			$.ajax({
				url: next_link,
				success: function( response ){
					$(response).find( '.post-item' ).each(function(){
						var $$this = $(this);
						if( counter == max_in_row ){
							$('.ajax-container').append( '<div class="row"></div>' );
							counter = 0;
						}
						var col = 12;
						if( max_in_row !== 12 ){
							col = 12 / max_in_row;
						}
						counter++;
						$('.ajax-container .row:last').append( '<div class="col-md-'+col+' post-item" style="'+$$this.attr('style')+'">'+$$this.html()+'</div>' );
					});
					
					var $link = $(response).find( '.load-more' ).val();
					if( $link != "" ){
						$this.val( $link );
					}
					else{
						$this.remove();
					}
				},
				complete: function(){
					calling = false;
				}
			});
		}
	});	

	/* SUBMIT FORMS */
	$('.submit_form').click(function(){
		$(this).parents('form').submit();
	});
	
	
	/* SUBSCRIBE */
	$('.subscribe').click( function(e){
		e.preventDefault();
		var $this = $(this);
		var $parent = $this.parents('.subscribe-form');		
		
		$.ajax({
			url: ajaxurl,
			method: "POST",
			data: {
				action: 'subscribe',
				email: $parent.find( '.email' ).val()
			},
			dataType: "JSON",
			success: function( response ){
				if( !response.error ){
					$parent.find('.sub_result').html( '<div class="alert alert-success" role="alert"><span class="fa fa-check-circle"></span> '+response.success+'</div>' );
				}
				else{
					$parent.find( '.sub_result' ).html( '<div class="alert alert-danger" role="alert"><span class="fa fa-times-circle"></span> '+response.error+'</div>' );
				}
			}
		})
	} );
	
	
	/* handle likes*/
	$(document).on('click', '.post-like', function(e){
		e.preventDefault();
		var $this = $(this);
		var post_id = $this.data('post_id');
		
		$.ajax({
			url: ajaxurl,
			method: "POST",
			dataType: "JSON",
			data: {
				action: 'likes',
				post_id: post_id
			},
			success: function( response ){
				if( !response.error ){
					$this.find('.like-count').text( response.count );
				}
				else{
					alert( response.error );
				}
			}
		});
	});	
		
	/* contact script */
	$('.send-contact').click(function(e){
		e.preventDefault();
		
		$.ajax({
			url: ajaxurl,
			method: "POST",
			data: {
				action: 'contact',
				name: $('.name').val(),
				email: $('.email').val(),
				subject: $('.subject').val(),
				message: $('.message').val()
			},
			dataType: "JSON",
			success: function( response ){
				if( !response.error ){
					$('.send_result').html( '<div class="alert alert-success" role="alert"><span class="fa fa-check-circle"></span> '+response.success+'</div>' );
				}
				else{
					$('.send_result').html( '<div class="alert alert-danger" role="alert"><span class="fa fa-times-circle"></span> '+response.error+'</div>' );
				}
			}
		})
	});
	
	/* MAGNIFIC POPUP FOR THE GALLERY */
	$('.gallery').each(function(){
		var $this = $(this);
		$this.magnificPopup({
			type:'image',
			delegate: 'a',
			gallery:{enabled:true},
		});
	});
	
	/* MAIN SEARCH BAR */
	$('.search-bar').click(function(){
		$('.main-search').animate( {top: '0px', opacity: '1'}, 200 );
	});
	
	$('.main-search').click(function(e){
		if( $(e.target).hasClass('main-search') ){
			$('.main-search').animate( {top: '-2000px', opacity: '0'}, 200 );
		}
	});

	$('.post-item').each(function(){
		var
			// Url elemento
			url = $(this).children('.post-item-content').children('.post-title').attr('href'),
			// Botão Lightbox
			btn = '<a href="'+url+'" class="btn"></a>';

		if( $(this).hasClass('format-video') ){
			$(this).append( btn);
		}

	}).on('mouseover', function(){
		if( !$(this).children('.btn').is(':animated') ){
			$(this).children('.btn').show();
		}
	}).on('mouseout', function(){
		if( !$(this).children('.btn').is(':animated') ){
			$(this).children('.btn').hide();
		}
	});

	// Embeds de vídeo internas

	// UOL mais
    $('embed').each(function(i,e) {
    	var
    		// Largura
    		width = $(e).width(),
    		height = $(e).width()/1.777;
        if(e.src.indexOf('mais.uol.com.br/embed') > 0) {
            var m = /mediaId=([0-9]+)/.exec(e.src);
            
            if(m) {
                $(e).parent().parent().append(
                    '<video poster="http://thumb.mais.uol.com.br/' 
                    + m[1] 
                    +'.jpg?ver=1" src="http://storage.mais.uol.com.br/'
                    + m[1] +'.mp4?ver=1" type="video/mp4" width="'+width+'"" height="'+height+'" class="web mobile" controls autoplay autobuffer />');
            }
        }
    });


	$('.container iframe, .container video').each(function(){
		var
			// Largura da janela
			width = $('.container').width()-30,
			// Largura maxima dos iframes
			maxWidth = $(this).width();

		$(this).css({ 'max-width': maxWidth });
		if( width <= 970  ){
			$(this).css({ 'width':width, 'height': $(this).width()/1.777 });
		}
	});

	$(window).resize(function(){
		var
			// Largura da janela
			width = $('.container').width();

		$('.container iframe, .container video').each(function(){
			var
				height = $(this).width()/$(this).height();

			$(this).css({ 'width':width, 'height': $(this).width()/1.777 });
				console.log($(this).height());
		});

	});

	// Omniture Galerias
	var omnitureGalerias = function(){

		if( $('body.single').find('.mfp-gallery') ){
			$(document).keydown(function(e){
				var photo = $('body.single').find('.mfp-gallery img').attr('src');
				if(e.keyCode == 39){
					if(typeof hitOmniturePhoto!="undefined"){
						hitOmniturePhoto( photo );
					}
				} else if(e.keyCode == 37){
					if(typeof hitOmniturePhoto!="undefined"){
						hitOmniturePhoto( photo );
					}
				};
			});
		}
		
		$(document).on('click','button.mfp-arrow',function(){
			var photo = $('body.single').find('.mfp-gallery img').attr('src');
			if(typeof hitOmniturePhoto!="undefined"){
				hitOmniturePhoto( photo );
			}
		});

		$('.gallery-item','.gallery').on('click',function(){
			var photo = $(this).attr('href');
			if(typeof hitOmniturePhoto!="undefined"){
				hitOmniturePhoto( photo );
			}
		});

	}
	omnitureGalerias();

});