<form role="search" method="get" class="searchform" action="<?php echo esc_url( home_url('/') ); ?>">
	<div class="blogismo-form">
		<input type="text" value="" name="s" class="form-control" placeholder="Buscar por...">
		<a class="btn btn-default submit_form"><i class="fa fa-search"></i></a>
	</div>
</form>