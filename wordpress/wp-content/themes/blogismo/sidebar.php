<?php
if ( is_active_sidebar( 'blog-1' ) || is_active_sidebar( 'blog-2' ) || is_active_sidebar( 'blog-3' ) ){
?>
<section class="widget-section <?php echo get_comments_number() > 0 ? '' : 'even' ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<?php 
				if ( is_active_sidebar( 'blog-2' ) ){
					dynamic_sidebar( 'blog-2' ); 
				}
				?>
			</div>
			<div class="col-md-4" style="padding:0!important;">
				<div class="widget widget_custom_posts">
					<div class="widget-title-wrap" style="margin-bottom:23px">
						<h6 class="widget-title">Social</h6>
					</div>
					<div class="fb-page" data-href="https://www.facebook.com/unimundo21" data-width="323" data-height="300" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/unimundo21"><a href="https://www.facebook.com/unimundo21">Unimundo21</a></blockquote></div></div>
				</div>
				<!--?php 
				if ( is_active_sidebar( 'blog-1' ) ){
					dynamic_sidebar( 'blog-1' ); 
				}
				?-->
			</div>
			<div class="col-md-4">
				<div id="banner-300x250" class="tm-ads"><script type="text/javascript">TM.display();</script></div>
				<!--?php 
				if ( is_active_sidebar( 'blog-3' ) ){
					dynamic_sidebar( 'blog-3' ); 
				}
				?-->
			</div>
		</div>
	</div>
</section>
<?php
}
?>