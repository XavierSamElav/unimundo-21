<?php
/*
	Template Name: Page Contact
*/
get_header();
the_post();
?>

<section class="main-listing">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if( has_post_thumbnail() ): ?>
					<div class="embed-responsive embed-responsive-16by9">
						<?php  the_post_thumbnail( 'full', array( 'class' => 'embed-responsive-item' ) ); ?>
					</div>
				<?php endif; ?>

				<div class="post-content">
					<div class="row">
						<div class="col-md-6">

							<form id="comment-form" class="comment-form contact-form">
								<div class="form-group has-feedback">
									<input type="text" class="form-control name" id="name" name="name" placeholder="<?php esc_attr_e( 'Your name', 'blogismo' ) ?>" />
								</div>
								<div class="form-group has-feedback">
									<input type="text" class="form-control email" id="email" name="email" placeholder="<?php esc_attr_e( 'Your email', 'blogismo' ) ?>" />
								</div>
								<div class="form-group has-feedback">
									<input type="text" class="form-control subject" id="subject" name="subject" placeholder="<?php esc_attr_e( 'Message subject', 'blogismo' ) ?>" />
								</div>
								<div class="form-group has-feedback">
									<textarea rows="10" cols="100" class="form-control message" id="message" name="message" placeholder="<?php esc_attr_e( 'Your message', 'blogismo' ) ?>"></textarea>
								</div>
								<p class="form-submit">
									<a href="javascript:;" class="send-contact btn"><?php _e( 'Enviar', 'blogismo' ) ?> </a>
								</p>
								<div class="send_result"></div>
							</form>

						</div>
						<div class="col-md-6">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_sidebar('page'); ?>
<?php get_footer(); ?>