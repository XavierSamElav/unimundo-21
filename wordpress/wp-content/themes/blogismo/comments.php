<?php
	/**********************************************************************
	***********************************************************************
	BLOGISMO COMMENTS
	**********************************************************************/	
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ( 'Please do not load this page directly. Thanks!' );
	if ( post_password_required() ) {
		return;
	}
?>
<?php if ( comments_open() ) :?>
	<?php if( have_comments() ):?>
		<section class="main-listing">
			<div class="container">
				<div class="row">
					<div class="col-md-12">	
						<!-- title -->
						<div class="widget-title-wrap">
							<h6 class="widget-title">
								<?php comments_number( __( 'No Comments', 'blogismo' ), __( '1 Comment', 'blogismo' ), __( '% Comments', 'blogismo' ) ); ?>
							</h6>
						</div>
						<!--.title -->
					
						<!-- comments -->
						<div class="comment-content comments">
							<?php if( have_comments() ):?>
								<?php wp_list_comments( array(
									'type' => 'comment',
									'callback' => 'blogismo_comments',
									'end-callback' => 'blogismo_end_comments',
									'style' => 'div'
								)); ?>
							<?php endif; ?>
						</div>
						<!-- .comments -->
					
						<!-- comments pagination -->
						<?php
							$comment_links = paginate_comments_links( 
								array(
									'echo' => false,
									'type' => 'array',
									'prev_next' => false,
									'separator' => ' ',
								) 
							);
							if( !empty( $comment_links ) ):
						?>
							<div class="comments-pagination-wrap">
								<div class="pagination">
									<?php _e( 'Comment page: ', 'blogismo');  echo blogismo_format_pagination( $comment_links ); ?>
								</div>
							</div>
						<?php endif; ?>
						<!-- .comments pagination -->
				</div>
			</div>
		</div>
	</section>						
	<?php endif; ?>
	
	<!-- leave comment form -->
	<section class="<?php echo get_comments_number() > 0 ? 'even no-bottom-margin' : '' ?>">
		<div class="container">
			<div class="row">
				<div class="col-md-12">	
	
					<!-- title -->
					<div class="widget-title-wrap">
						<h6 class="widget-title">Deixe um comentário</h6>
					</div>
					<!--.title -->
					<div id="contact_form">
						<?php
							$comments_args = array(
								'id_form'		=> 'comment-form',
								'label_submit'	=>	'Enviar comentário',
								'title_reply'	=>	'',
								'fields'		=>	apply_filters( 'comment_form_default_fields', array(
														'author' => '<div class="form-group has-feedback">
																		<input type="text" class="form-control" id="name" name="author" placeholder="Nome">
																	</div>',
														'email'	 => '<div class="form-group has-feedback">
																		<input type="email" class="form-control" id="email" name="email" placeholder="Email">
																	</div>'
													)),
								'comment_field'	=>	'<div class="form-group has-feedback">
														<textarea rows="10" cols="100" class="form-control" id="message" name="comment" placeholder="Comentário"></textarea>															
													</div>',
								'cancel_reply_link' => __( 'or cancel reply', 'coupon' ),
								'comment_notes_after' => '',
								'comment_notes_before' => ''
							);
							comment_form( $comments_args );	
						?>
					</div>
					<!-- content -->
				</div>
			</div>
		</div>
	</section>					
	<!-- .leave comment form -->
<?php endif; ?>