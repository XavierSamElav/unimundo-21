<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="keywords" content="<?php echo esc_attr( blogismo_get_option( 'seo_keywords' ) ) ?>"/>
    <meta name="description" content="<?php echo esc_attr( blogismo_get_option( 'seo_description' ) ) ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- Title -->
	<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'blogismo' ), max( $paged, $page ) );

	?></title>

    <!-- Favicon -->
	<?php
		$favicon = blogismo_get_option( 'site_favicon' );
		if( !empty( $favicon ) ):
	?>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo esc_url( $favicon ); ?>">
	<?php
		endif;
	?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Tag de Banner -->
	<?php if( is_home() || is_single() ){ ?>
		<script type="text/javascript">
			var TM = TM || {};
			TM.Config = {
		        'site' : 'par',
		            'affiliate' : 'parvirgula',
		            'chan' : 'parvirgulaunimundo',
		            'subchan' : '',
		            'campaignuol' : '1',
		            'group' : '6',
		            'keyword' : '',
		            'tags' : '',
		            'platform' : 'web',
		            'banners' : <?php if( !is_home() ){ echo "['banner-728x90', 'banner-300x250', 'banner-728x90-2']"; }else{ echo "['banner-728x90']"; } ?>
			};
		</script>
		<script src="http://tm.uol.com.br/h/par/parceiros.js"></script>
	<?php } ?>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<!-- tag de Banner-->
<script src="http://tm.uol.com.br/b/par/parceiros.js"></script>

<div id="barraCPSContainer">
		<!-- Barra Virgula/UOL -->
		<script type="text/javascript" src="http://barras.virgula.uol.com.br/media/js/v2.js"></script>
</div>

<!--div class="spinner-wrap">
	<span class="loader"><span class="loader-inner"></span></span>
</div-->
<!-- ==================================================================================================================================
TOP BAR
======================================================================================================================================= -->
<?php
$background = '';
if( !is_home() && has_post_thumbnail() && !is_search() ){
	$image_data = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
	$background = 'background-image: url( '.$image_data[0].' )!important;';
}
?>
<section class="top-bar" style="<?php echo esc_attr( $background ); ?>">
	<div class="top-bar-overlay"></div>
	<div class="main-search">
		<?php get_search_form(); ?>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ 'top-navigation' ] ) ): ?>
					<div id="navigation">
						<button class="navbar-toggle button-white menu" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only"><?php _e( 'Toggle navigation', 'blogismo' ) ?></span>
							<i class="fa fa-bars fa-3x"></i>
						</button>
						<div class="navbar navbar-default" role="navigation">
							<div class="collapse navbar-collapse">
								<?php
								wp_nav_menu( array(
									'theme_location'  	=> 'top-navigation',
									'menu_class'        => 'nav navbar-nav',
									'container'			=> false,
									'echo'          	=> true,
									'items_wrap'        => '<ul class="%2$s">%3$s<li><a href="javascript:;" class="search-bar"><i class="fa fa-search"></i></a></li></ul>',
									'walker' 			=> new blogismo_walker
								) );
								?>
							</div>
						</div>
						<div class="social">
							<a href="https://www.facebook.com/unimundo21/" class="fb ir" target="_blank">Facebook</a>
							<a href="https://instagram.com/unimundo21/" class="instagram ir nomargin" target="_blank">Instagram</a>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">

			</div>
		</div>
	</div>
	<div class="blog-title">
		<h1>
			<?php
				if ( is_category() ){?>
					<!-- echo __('Category: ', 'blogismo'); -->
					<img src='<?php bloginfo( 'template_url' )?>/images/logo.png' alt='Unimundo 21' width="182">
				<?php	single_cat_title();
				}
				else if( is_404() ){
					_e( '404 Page Not Found', 'blogismo' );
				}
				else if( is_tag() ){
					echo __('Search by tag: ', 'blogismo'). get_query_var('tag');
				}
				else if( is_author() ){
					echo __('Posts written by ', 'blogismo'). get_the_author_meta( 'display_name' );

				}
				else if( is_archive() ){
					echo __('Archive for ', 'blogismo'). single_month_title(' ',false);
				}
				else if( is_search() ){
					echo 'Resultados da busca por: '. get_search_query();
				}
				else if( is_home() ){?>
					<img src='<?php bloginfo( 'template_url' )?>/images/logo.png' alt='Unimundo 21' class="logo">
				<?php
				}
				else if( is_singular( 'post' ) ){?>
					<img src='<?php bloginfo( 'template_url' )?>/images/logo.png' alt='Unimundo 21' width="182">
				<?php the_title();
				}
				else{?>
					<img src='<?php bloginfo( 'template_url' )?>/images/logo.png' alt='Unimundo 21' width="182">
					<?php	the_title();?>
					<p><?php echo get_the_excerpt(); ?></p>
				<?php }	?>
		</h1>
		<?php
		if( is_singular( 'post' ) ){
			?>
			<ul class="list-unstyled post-meta post-meta-single">
				<li>Por <?php the_author(); ?></li>
				<li>
					<?php
						$category = get_the_category();
						$catname = $category[0]->cat_name;
						$catslug = $category[0]->slug;
					?>
					<a href="<?php echo home_url('category/'.$catslug); ?>"><?php echo $catname; ?></a>
				</li>
				<!-- <li>
					<a href="javascript:;" class="post-like" data-post_id="<?php the_ID(); ?>"><i class="fa fa-heart"></i> <span class="like-count"><?php echo blogismo_get_post_extra( 'likes' );?></span></a>
				</li>
				<li>
					<i class="fa fa-eye"></i> <?php echo blogismo_get_post_extra( 'views' );?>
				</li>
				<li>
					<i class="fa fa-calendar"></i> <?php the_time( 'F j, Y' ) ?>
				</li>
				<li>
					<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><i class="fa fa-user"></i> <?php echo get_the_author_meta('display_name'); ?></a>
				</li>
				<li>
					<i class="fa fa-pencil"></i> <?php echo blogismo_the_category(); ?>
				</li> -->
			</ul>
			<?php
		}
		?>
		<small>
			<?php
				if( is_home() ){
					// echo $site_description;
				}
				else if( is_404() ){
					_e( 'Sorry but we could not find what you are looking for', 'blogismo' );
				}
			?>
		</small>

		


		<!-- Begin MailChimp Signup Form -->
		<link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
		<style type="text/css">
			#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
			/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
			   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
		</style>
		<div id="mc_embed_signup" style="background:transparent;">
		<form action="//uol.us12.list-manage.com/subscribe/post?u=fef461d9bdccce56625edd8fe&amp;id=cb82dbd00a" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
		    <div id="mc_embed_signup_scroll">
			<label for="mce-EMAIL">Assine nossa newsletter</label>
			<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Endereço de e-mail" required>
		    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_fef461d9bdccce56625edd8fe_cb82dbd00a" tabindex="-1" value=""></div>
		    <div class="clear"><input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
		    </div>
		</form>
		</div>
		<!--End mc_embed_signup-->
	</div>
</section>

