<?php
if ( is_active_sidebar( 'page-1' ) || is_active_sidebar( 'page-2' ) || is_active_sidebar( 'page-3' ) ){
?>
<section class="widget-section <?php echo get_comments_number() > 0 ? '' : 'even' ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<?php 
				if ( is_active_sidebar( 'page-1' ) ){
					dynamic_sidebar( 'page-1' ); 
				}
				?>
			</div>
			<div class="col-md-4">
				<?php 
				if ( is_active_sidebar( 'page-2' ) ){
					dynamic_sidebar( 'page-2' ); 
				}
				?>
			</div>
			<div class="col-md-4">
				<?php 
				if ( is_active_sidebar( 'page-3' ) ){
					dynamic_sidebar( 'page-3' ); 
				}
				?>
			</div>
		</div>
	</div>
</section>
<?php
}
?>