<?php
/*=============================
	DEFAULT SINGLE
=============================*/
the_post();
get_header();
$post_pages = wp_link_pages(
	array(
		'before' => '',
		'after' => '',
		'link_before'      => '<span>',
		'link_after'       => '</span>',
		'next_or_number'   => 'number',
		'nextpagelink'     => __( '&raquo;', 'blogismo' ),
		'previouspagelink' => __( '&laquo;', 'blogismo' ),
		'separator'        => ' ',
		'echo'			   => 0
	)
);

blogismo_increase_views();
?>
<section class="main-listing">
	<div class="container">
		<div id="banner-728x90" class="tm-ads"><script type="text/javascript">TM.display();</script></div>
		<div class="row">
			<div class="col-md-12">

				<div class="post-content">
					<?php the_content(); ?>
				</div>

				<?php
				$tags = blogismo_the_tags();
				if( !empty( $tags ) ):
				?>
					<div class="post-tags pull-left">
						<?php _e( '<i class="fa fa-tags"></i> Post tags: ', 'blogismo' ); echo $tags; ?>
					</div>
				<?php
				endif;
				?>

				<div class="post-share pull-right">
					<span>Compartilhe:</span>
					<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" class="share facebook" target="_blank" title="<?php esc_attr_e( 'Share on Facebook', 'blogismo' ); ?>"><i class="fa fa-facebook"></i></a>
					<a href="http://twitter.com/intent/tweet?source=<?php echo esc_attr( bloginfo( 'name' ) ); ?>&amp;text=<?php echo esc_attr( rawurlencode( get_the_excerpt() ) ); ?>&amp;url=<?php echo esc_attr( rawurlencode( get_permalink() ) ); ?>" class="share twitter" target="_blank" title="<?php esc_attr_e( 'Share on Twitter', 'blogismo' ); ?>"><i class="fa fa-twitter"></i></a>
					<a href="https://plus.google.com/share?url=<?php echo esc_url( rawurlencode( get_permalink() ) ); ?>" class="share google" target="_blank" title="<?php esc_attr_e( 'Share on Google+', 'blogismo' ); ?>"><i class="fa fa-google"></i></a>
					<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo esc_url( rawurlencode( get_permalink() ) ); ?>&amp;title=<?php echo esc_attr( rawurlencode( get_the_title() ) ); ?>&amp;summary=<?php echo esc_attr( rawurlencode( get_the_excerpt() ) ); ?>&amp;source=<?php echo esc_attr( bloginfo( 'name' ) ); ?>" class="share linkedin" target="_blank" title="<?php esc_attr_e( 'Share on LinkedIn', 'blogismo' ); ?>"><i class="fa fa-linkedin"></i></a>
					<a href="http://www.tumblr.com/share/link?url=<?php echo esc_url( rawurlencode( get_permalink() ) ); ?>&amp;name=<?php echo esc_attr( rawurlencode( get_the_title() ) ); ?>&amp;description=<?php echo esc_attr( rawurlencode( get_the_excerpt() ) ); ?>" class="share tumblr" target="_blank" title="<?php esc_attr_e( 'Share on Tumblr', 'blogismo' ); ?>"><i class="fa fa-tumblr"></i></a>
				</div>
				<div class="clearfix"></div>

				<?php if( !empty( $post_pages ) ): ?>
					<div class="pagination">
						<?php echo blogismo_link_pages( $post_pages ); ?>
					</div>
				<?php endif; ?>

				<?php
				$prev_post = get_previous_post();
				$next_post = get_next_post();
				if( !empty( $prev_post ) || !empty( $next_post ) ):
				?>
					<div class="next-prev">
						<div class="row">
							<div class="col-md-6 left-text">
								<?php
								if( !empty( $prev_post ) ){
									$prev_query = new WP_Query( array(
										'post_type' => 'post',
										'post__in' => array( $prev_post->ID ),
										'posts_per_page' => 1,
										'ignore_sticky_posts' => true
									) );
									if( $prev_query->have_posts() ){
										while( $prev_query->have_posts() ){
											$prev_query->the_post();
											?>
											<a href="<?php the_permalink() ?>" class="btn">
												&laquo; <?php the_title() ?>
											</a>
											<?php
										}
									}
									wp_reset_query();
								}
								?>
							</div>
							<div class="col-md-6 right-text">
								<?php
								if( !empty( $next_post ) ){
									$next_query = new WP_Query( array(
										'post_type' => 'post',
										'post__in' => array( $next_post->ID ),
										'posts_per_page' => 1,
										'ignore_sticky_posts' => true
									) );
									while( $next_query->have_posts() ){
										$next_query->the_post();
										?>
										<a href="<?php the_permalink() ?>" class="btn">
											<?php the_title() ?> &raquo;
										</a>
										<?php
									}
									wp_reset_query();
								}
								?>
							</div>
						</div>
					</div>
				<?php endif; ?>
				<div id="banner-728x90-2" class="tm-ads bottom"><script type="text/javascript">TM.display();</script></div>
			</div>
		</div>
	</div>
</section>
<?php get_sidebar(); ?>
<?php comments_template( '', true ) ?>
<?php get_footer(); ?>