<?php header('Content-type: text/css'); 
	/* HEADER */
	$header_bg_image = blogismo_get_option( 'header_bg_image' );
	$header_bg_color = blogismo_get_option( 'header_bg_color' );//#666666
	$header_overlay = blogismo_get_option( 'header_overlay' );//#000000
	$header_overlay_opacity = blogismo_get_option( 'header_overlay_opacity' );//0.2
	
	/* POST ITEMS */
	$post_item_overlay = blogismo_get_option( 'post_item_overlay' );//#000000
	$post_item_overlay_opacity = blogismo_get_option( 'post_item_overlay_opacity' );//0.4
	
	/* MAINCOLOR */
	$main_color = blogismo_get_option( 'main_color' ); //#95B045
	$maincolor_btn_font_clr = blogismo_get_option( 'maincolor_btn_font_clr' ); //#FFFFFF
	
	/* TITLE FONT */
	$title_font = blogismo_get_option( 'title_font' );//Roboto Slab
	
	/* TEXT FONT */
	$text_font = blogismo_get_option( 'text_font' );//Roboto Slab

?>

body{
	font-family: "<?php echo str_replace( "+", " ", $text_font ); ?>", sans-serif;
}

h1, h2, h3, h4, h5, h6{
	font-family: "<?php echo str_replace( "+", " ", $title_font ); ?>", sans-serif;
}

.top-bar{
	background-color: <?php echo $header_bg_color ?>;
	background-image: url(<?php echo $header_bg_image ?>);
}

.post-item-overlay{
	background-color: <?php echo $post_item_overlay ?>;
	opacity: <?php echo $post_item_overlay_opacity ?>;
}

.top-bar-overlay{
	background-color: <?php echo $header_overlay ?>;
	opacity: <?php echo $header_overlay_opacity ?>;
}

a, a:hover, a:focus, a:active, a:visited,
.post-meta a:hover, .post-tags a:hover,
.post-title:hover h2,
.nav.navbar-nav ul li.open > a,
.nav.navbar-nav ul li.open > a:hover,
.nav.navbar-nav ul li.open > a:focus,
.nav.navbar-nav ul li.open > a:active,
.nav.navbar-nav ul li.current > a,
.navbar-nav ul li.current-menu-parent > a, .navbar-nav ul li.current-menu-ancestor > a, .navbar-nav ul li.current-menu-item  > a,
.nav.navbar-nav ul li a:hover,
.navbar-toggle,
.comment-reply-link:hover,
.widget-small-link:hover{
	color: <?php echo $main_color; ?>;
}

@media only screen and (max-width: 768px) {
	.navbar-default .navbar-nav .open .dropdown-menu > li > a:hover, 
	.navbar-default .navbar-nav .open .dropdown-menu > li > a:focus,
	.navbar-default .navbar-nav .open .dropdown-menu > li > a:active
	.navbar-default .navbar-nav .open .dropdown-menu > li.current > a,
	.navbar-default .navbar-nav .open .dropdown-menu > li.current-menu-ancestor > a,
	.navbar-default .navbar-nav .open .dropdown-menu > li.current-menu-item > a,
	.navbar-default .navbar-nav .open .dropdown-menu > li.current-menu-parent > a{
		color: <?php echo $main_color; ?>;
	}
}

blockquote,
#navigation .nav.navbar-nav > li.open,
#navigation .nav.navbar-nav > li:hover,
#navigation .nav.navbar-nav > li:focus,
#navigation .nav.navbar-nav > li:active,
#navigation .nav.navbar-nav > li.current,
#navigation .navbar-nav > li.current-menu-parent, #navigation  .navbar-nav > li.current-menu-ancestor, #navigation  .navbar-nav > li.current-menu-item,
.widget-title,
.widget_custom_posts .nav-tabs li a:hover, .widget_custom_posts .nav-tabs li a:active, .widget_custom_posts .nav-tabs li a:focus{
	border-color: <?php echo $main_color; ?>;
}

table th,
.tagcloud a, .btn, a.btn,
.form-submit #submit,
.spinner-wrap,
.widget_custom_posts .nav-tabs li a:hover, .widget_custom_posts .nav-tabs li a:active, .widget_custom_posts .nav-tabs li a:focus,
.gallery-overlay{
	background: <?php echo $main_color; ?>;
	color: <?php echo $maincolor_btn_font_clr ?>;
}

.gallery-overlay{
	background: rgba( <?php echo blogismo_hex2rgb( $main_color ); ?>, 0.8 );
}

table th a, table th a:hover, table th a:focus, table th a:active, table th a:visited{
	color: <?php echo $maincolor_btn_font_clr ?>;
}