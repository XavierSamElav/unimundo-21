<?php
/*=============================
	DEFAULT PAGE
=============================*/
get_header();
the_post();
?>

<section class="main-listing">
	<div class="container">
		<div class="row">
			<div class="col-md-12">				
				<div class="post-content">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php comments_template( '', true ) ?>
<?php get_sidebar('page'); ?>
<?php get_footer(); ?>