<?php // if( is_home() ){?>
	<footer>
		<div id="banner-728x90" class="tm-ads bottom"><script type="text/javascript">TM.display();</script></div>
		<div class="container">
			<div class="fb-page web" data-href="https://www.facebook.com/unimundo21" data-width="380" data-height="230" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/unimundo21"><a href="https://www.facebook.com/unimundo21">Unimundo21</a></blockquote></div></div>
			<div class="fb-page mobile" data-href="https://www.facebook.com/unimundo21" data-width="320" data-height="302" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"></div>
			<img src='<?php bloginfo( 'template_url' )?>/images/logo.png' alt='Unimundo 21'>

			<p>Unimundo21 é o começo de algo inédito no Brasil: jornalismo ambiental com notícias apenas e tão somente POSITIVAS sobre o nosso planetinha e como interagimos com ele. Prepare-se para conteúdos interessantes, leves, curiosos e úteis para a vida. Apareça aqui naquela sua pausa boa para curtir uma good vibe!</p>
			<div class="social">
				<a href="https://www.facebook.com/unimundo21/" class="fb ir" target="_blank">Facebook</a>
				<a href="https://instagram.com/unimundo21/" class="instagram ir nomargin" target="_blank">Instagram</a>
			</div>
			<a href="http://www.elav.com.br/?source=unimundo21" target="_blank" class="elav ir">elav</a>
		</div>
	</footer>
<?php // } ?>
<div id="lightbox"></div>

<a href="javascript:;" class="to_top btn">
	<span class="fa fa-angle-double-up fa-2x"></span>
</a>

	<!-- Script Page box - Facebook -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.3";
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

	<!-- Script Google Analytics -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-63205574-1', 'auto'); // Tracker do Unimundo 21
	  ga('send', 'pageview'); // Envia pageview para o tracker do Unimundo 21
	  ga('create', 'UA-2498222-7', 'auto', {'name': 'virgula'}); // Tracker do Virgula
	  ga('virgula.send', 'pageview'); // Envia pageview para o tracker do Virgula

	</script>

	<script>
	  var omnitureParams = window.omnitureParams||{};
	  omnitureParams.partner = ['lifestyle', 'unimundo21'];
	</script>
	 
	<!-- SiteCatalyst code version: H.27.2. Copyright 1997-2014 Omniture, Inc. More info available at http://www.omniture.com-->
	<script language="JavaScript" type="text/javascript" charset="iso-8859-1" src="http://me.jsuol.com.br/omtr/virgula.js"></script>
	<script language="JavaScript" type="text/javascript"><!--
	/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
	var s_code=uol_sc.t();if(s_code)document.write(s_code)//--></script>
	<!-- End SiteCatalyst code version: H.27.2. -->

<?php
wp_footer();
?>