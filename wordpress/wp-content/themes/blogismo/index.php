<?php
/*=============================
	DEFAULT BLOG LISTING PAGE
=============================*/
get_header();
global $wp_query;

$cur_page = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; //get curent page
$page_links_total =  $wp_query->max_num_pages;
$page_links = paginate_links( 
	array(
		'base' => add_query_arg( 'paged', '%#%' ),
		'prev_next' => true,
		'end_size' => 2,
		'mid_size' => 2,
		'total' => $page_links_total,
		'current' => $cur_page,	
		'prev_next' => false,
		'type' => 'array'
	)
);	
$pagination = blogismo_format_pagination( $page_links );
$random_post_colors = blogismo_get_option( 'random_post_colors' );
$random_post_colors = explode( ',', $random_post_colors );
?>
<section class="main-listing">	
	<?php 
	if( have_posts() ){
		?>
		<div class="container-fluid">
			<div class="ajax-container">
				<div class="row">
					<?php
					$counter = 0;
					$max = blogismo_get_option( 'blog_columns' );
					if( empty( $max ) ){
						$max = 3;
					}
					$col = 12 / $max;
					?>
					<input type="hidden" class="max_in_row" value="<?php echo $max; ?>">
					<?php
					while( have_posts() ){
						the_post();

						$category = get_the_category(); 
						$catname  = $category[0]->cat_name;
						$catslug  = $category[0]->slug;
						$format 	 = get_post_format();

						if( $counter == $max ){
							?>
							</div>
							<div class="row">
							<?php
							$counter = 0;
						}
						$counter++;
						$url = blogismo_get_avatar_url( get_avatar( get_the_author_meta( 'ID' ), 50 ) );
						
						$post_meta = get_post_meta( get_the_ID() );
						$bg_color = blogismo_get_smeta( 'bg_color', $post_meta, '' );
						
						$background = '';
						if( !empty( $bg_color ) ){
							$background = 'background-color: '.$bg_color.'; ';
						}
						else if( !empty( $random_post_colors ) ){
							$background = 'background-color: '.$random_post_colors[array_rand( $random_post_colors, 1 )].'; ';
						}
						
						if( has_post_thumbnail() ){
							$bg_image_data = wp_get_attachment_image_src( get_post_thumbnail_id(), 'post-thumbnail' );
							$background .= 'background-image: url( '.$bg_image_data[0].' );';
						}					
						?>
								<div <?php post_class( 'col-md-'.$col.' post-item' ) ?> style="<?php echo esc_attr( $background ); ?>">
								<div class="post-item-overlay">
									<div class="post-item-border"></div>
								</div>
								<div class="post-item-content">
									<p>

										<a href="<?php echo home_url('category/'.$catslug); ?>"><?php echo $catname; ?></a>
									</p>
									<?php blogismo_the_title(); ?>
								</div>
							</div>
						<?php
					}
					?>
				</div>
			</div>
			<?php
			if( !empty( $pagination ) ){
				$next = get_next_posts_link();
				$temp = explode( "href=\"", $next );
				$temp2 = explode( "\"", $temp[1] );
				?> 
					<input type="hidden" class="load-more" value="<?php echo esc_url( $temp2[0] ); ?>">
				<?php
			}
			?>
		</div>
		<?php
	}
	else{
		?>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="post-content">
						<!-- title -->
						<div class="widget-title-wrap">
							<h6 class="widget-title">Sem resultados</h6>
						</div>
						<!--.title -->
						<p>Não encontramos nenhum resultado com o seu critério de busca. Por favor, tente novamente utilizando o campo abaixo.</p>
						<?php get_search_form(); ?>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	?>
	
</section>
<!-- .pagination -->
<?php wp_reset_query(); ?>
<?php get_footer(); ?>