<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
switch($_SERVER['SERVER_NAME']){
	case 'unimundo-21.dev':
		define('WP_HOME','http://unimundo-21.dev/');
		define('WP_SITEURL','http://unimundo-21.dev/wordpress');

		define('DB_NAME', 'unimundo');
		define('DB_USER', 'elav');
		define('DB_PASSWORD', 'seguranca');
		define('DB_HOST', '192.168.0.2');
		define('DB_CHARSET', 'utf8');
		define('DB_COLLATE', '');
	break;
	case 'localhost':
		define('WP_HOME','http://localhost/unimundo-21');
		define('WP_SITEURL','http://localhost/unimundo-21/wordpress');

		define('DB_NAME', 'unimundo');
		define('DB_USER', 'elav');
		define('DB_PASSWORD', 'seguranca');
		define('DB_HOST', '192.168.0.2');
		define('DB_CHARSET', 'utf8');
		define('DB_COLLATE', '');
	break;
	case 'http://dev.unimundo21.xhtm.com.br/':
		define('WP_HOME','http://dev.unimundo21.xhtm.com.br');
		define('WP_SITEURL','http://dev.unimundo21.xhtm.com.br');

		define('DB_NAME', 'unimundodev');
		define('DB_USER', 'userunimundodev');
		define('DB_PASSWORD', 'unimundodev');
		define('DB_HOST', '200.98.146.231:3310');
		define('DB_CHARSET', 'utf8');
		define('DB_COLLATE', '');
	break;
	default:
		define('WP_HOME','http://dev.unimundo21.xhtm.com.br');
		define('WP_SITEURL','http://dev.unimundo21.xhtm.com.br');

		define('DB_NAME', 'unimundodev');
		define('DB_USER', 'userunimundodev');
		define('DB_PASSWORD', 'unimundodev');
		define('DB_HOST', '200.98.146.231:3310');
		define('DB_CHARSET', 'utf8');
		define('DB_COLLATE', '');
	break;
}

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A ^?n1Y]jO:}+j*rnSTViWGtopBXyKDf;Uk4AqRu(H4-BO?vX2hj2`m<@i _eV4A');
define('SECURE_AUTH_KEY',  '|A@*fp)}zRrApa5U>3U07[L)&;]Z-Gzi>!!{~[WJctH);Th[d[)%{Q C(`kPkHTI');
define('LOGGED_IN_KEY',    'Wn}SCSj+1Q=p4(,&#c@l@%h+hFtN%boAN+C7U~bA!y~?AX)#)7]?1aRI1maw/GE:');
define('NONCE_KEY',        'es<6!K|#RO,ym|5Q{,D/^S*;hFP^ljn>>6{++|Bbr4=G60UYsMn PG.Sz7e_QA/e');
define('AUTH_SALT',        '7^|PEgSN!VR_] @iReSv[S!L?Fv6Q(}4j0~f<2mr[hV+H`j[p28|G;>C?yE#.$Mh');
define('SECURE_AUTH_SALT', '+(|y}IB-L(bW .,:!|YE(:7^{HO<#TeOCq L-)*&1O7NCZE+,[*H+%j3GecxwNrg');
define('LOGGED_IN_SALT',   'N;7q=}|OCy_[6}q/J2Xs97i8!G{R<+-uA;E,k#:f!vUADqT8$D}B++=Fd|$}lx:Y');
define('NONCE_SALT',       'o7+%@~V>QUaOa.Eh.Nd=@UJ%^0<gE2.&ZuR/0rcX$46QM/*(0Dd  Vl?<Rnz(_bR');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
